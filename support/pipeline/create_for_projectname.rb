# frozen_string_literal: true

class CreateForProjectname
  class << self
    def create_pipeline
      project_name = ENV.fetch('PROJECT_NAME', nil)
      root_user = User.find_by_username('root')
      project = Project.find_by_path(project_name)

      if project
        pipeline_service = Ci::CreatePipelineService.new(project, root_user, ref: project.default_branch_or_main)
        pipeline = pipeline_service.execute(:api)

        if pipeline
          puts "Pipeline created successfully. Pipeline ID: \#{pipeline.payload.id}"
        else
          puts "Failed to create pipeline"
          exit 1
        end
      else
        puts "Project not found: \#{project_name}"
        exit 1
      end
    end
  end
end

CreateForProjectname.create_pipeline
