# frozen_string_literal: true

module Gitlab
  module QA
    module Scenario
      module Test
        module Omnibus
          class UpdateToNext < UpdateFromPrevious
            using Rainbow
            # Test upgrade from development version to next stable release
            #
            # @example
            # perform(gitlab-ee:dev-tag, 17.8.2-pre)
            # => will perform upgrade gitlab-ee:dev-tag -> 17.9.2
            #
            # @param [String] release current release docker image with development changes
            # @param [String] current_version current gitlab version associated with docker image
            # @param [String] from_edition gitlab edition to update from
            # @param [Array] *rspec_args rspec arguments
            # @return [void]
            def perform(release, current_version, from_edition = nil, *rspec_args)
              # When from_edition isn't actually passed but RSpec args arg passed with `-- rspec_args...`,
              # from_edition is wrongly set to `--`, so we fix that here.
              if from_edition == "--"
                rspec_args.prepend('--')
                from_edition = nil
              end

              @current_release = QA::Release.new(release)
              @upgrade_path = Support::GitlabUpgradePath.new(
                current_version,
                'from_patch',
                from_edition || @current_release.edition
              ).fetch
              @rspec_args = rspec_args

              # Key difference to UpdateFromPrevious: current_release comes first in the upgrade path
              upgrade_info = "(#{current_version}) #{[current_release, *upgrade_path].join(' => ')}".bright
              Runtime::Logger.info("Performing gitlab update: #{upgrade_info}")

              update(rspec_args)
            end

            private

            # Override to run specs in reverse order - first on development version, then on target version
            #
            # @param [Array] rspec_args
            # @return [void]
            def update(rspec_args)
              Docker::Volumes.new.with_temporary_volumes do |volumes|
                # deploy development release and run specs to populate db
                Runtime::Logger.info("Running the development release: #{current_release}")
                run_gitlab(current_release, volumes, seeding_suite_args, seeding_run: true)

                # deploy target release and run tests
                upgrade_path.each do |release|
                  Runtime::Logger.info("Upgrading GitLab to target release: #{release}")
                  run_gitlab(release, volumes, rspec_args, skip_setup: true)
                end
              end
            end

            # Override to run specs on development release and final target
            #
            # @param [Gitlab::QA::Release] release
            # @return [Boolean]
            def run_specs?(release)
              [current_release, upgrade_path.last].any?(release)
            end
          end
        end
      end
    end
  end
end
