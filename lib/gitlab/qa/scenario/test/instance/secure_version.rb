# frozen_string_literal: true

module Gitlab
  module QA
    module Scenario
      module Test
        module Instance
          class SecureVersion < Scenario::Template
            include Support::Shellout

            attr_writer :volumes, :seed_admin_token

            def initialize
              @volumes = {}
              @seed_admin_token = true
              @pipeline_ops = Component::PipelineOps.new
              @runner_ops = Component::RunnerOps.new
              @importer = Component::ProjectImporter.new(
                ENV.fetch('PROJECT_URL', nil),
                ENV.fetch('PROJECT_NAME', nil))
              @license_ops = Component::LicenseOps.new
            end

            def perform(release, *_rspec_args)
              Component::Gitlab.perform do |gitlab|
                setup_gitlab(gitlab, release)
                execute_gitlab_operations(gitlab)
              end
            end

            private

            def setup_gitlab(gitlab, release)
              gitlab.release = release
              gitlab.volumes = @volumes
              gitlab.network = Runtime::Env.docker_network
              gitlab.name = Runtime::Env.qa_gitlab_hostname
              gitlab.seed_admin_token = @seed_admin_token
              gitlab.tls = Runtime::Env.qa_gitlab_use_tls?
              # gitlab.exec_commands = foo
            end

            def execute_gitlab_operations(gitlab)
              gitlab.instance do
                @license_ops.add_license(gitlab)
                @runner_ops.setup_runner(gitlab)
                @importer.import_project(gitlab)
                @pipeline_ops.start(gitlab, 'secure-matrix-test')
                @pipeline_ops.check_status(gitlab)
              end
            end
          end
        end
      end
    end
  end
end
