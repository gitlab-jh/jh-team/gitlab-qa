# frozen_string_literal: true

describe Gitlab::QA::Release do
  let(:specific_ce_tag) { '10.8.4-ce.0' }
  let(:specific_ce_qa_tag) { '10.8.4-ce' }
  let(:specific_ee_tag) { '11.0.0-rc5.ee.1' }
  let(:specific_ee_qa_tag) { '11.0.0-rc5-ee' }
  let(:specific_version_timestamp_refs_tag) { '12.4.201910071518-933febe7c6d.aae3944cae6' }
  let(:gitlab_ref) { '933febe7c6d' }
  let(:full_ce_address) { 'registry.gitlab.com:5000/foo/gitlab/gitlab-ce' }
  let(:full_ce_address_with_simple_tag) { "#{full_ce_address}:latest" }
  let(:full_ce_address_with_complex_tag) { "#{full_ce_address}:#{specific_ce_tag}" }
  let(:full_ee_address) { 'registry.gitlab.com:5000/foo/gitlab/gitlab-ee' }
  let(:full_ee_address_with_simple_tag) { "#{full_ee_address}:latest" }
  let(:full_ee_address_with_complex_tag) { "#{full_ee_address}:#{specific_ee_tag}" }
  let(:dev_address) { 'dev.gitlab.org:5005/gitlab/omnibus-gitlab/gitlab-ee' }
  let(:full_dev_address) { "#{dev_address}:latest" }
  let(:full_dev_address_with_version_timestamp_refs_tag) { "#{dev_address}:#{specific_version_timestamp_refs_tag}" }
  let(:ce_image) { 'gitlab/gitlab-ce' }
  let(:ee_image) { 'gitlab/gitlab-ee' }
  let(:ce_qa_image) { "#{ce_image}-qa" }
  let(:full_ce_qa_image) { "#{full_ce_address}-qa" }
  let(:full_ee_qa_image) { "#{full_ee_address}-qa" }
  let(:dev_qa_address) { "#{dev_address}-qa" }
  let(:omnibus_mirror_image) do
    "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:db5d4783b4689cb78c32f613b8888e06684097e5"
  end

  describe '#initialize' do
    context 'when release is valid' do
      subject(:release) { described_class.new('ce') }

      it 'does not raise an error' do
        expect { release }.not_to raise_error described_class::InvalidImageNameError
      end
    end

    context 'when release is invalid' do
      subject(:release) { described_class.new('--') }

      it 'raises an error' do
        expect { release }.to raise_error described_class::InvalidImageNameError
      end
    end
  end

  describe '#to_s' do
    context 'when release is ce' do
      subject(:release) { described_class.new('ce') }

      it { expect(release.to_s).to eq 'gitlab/gitlab-ce:nightly' }
    end

    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.to_s).to eq 'gitlab/gitlab-ce:nightly' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.to_s).to eq 'gitlab/gitlab-ee:nightly' }
    end

    context 'when release is EDITION:tag' do
      subject(:release) { described_class.new("CE:#{specific_ce_tag}") }

      it { expect(release.to_s).to eq "gitlab/gitlab-ce:#{specific_ce_tag}" }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.to_s).to eq "gitlab/gitlab-ce:#{specific_ce_tag}" }
    end

    context 'when release is a full CE address' do
      context 'without a tag' do
        subject(:release) { described_class.new(full_ce_address) }

        it { expect(release.to_s).to eq full_ce_address_with_simple_tag }
      end

      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.to_s).to eq full_ce_address_with_simple_tag }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.to_s).to eq full_ce_address_with_complex_tag }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.to_s).to eq full_ee_address_with_simple_tag }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.to_s).to eq full_ee_address_with_complex_tag }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.to_s).to eq "#{ce_image}:latest" }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.to_s).to eq "#{full_ce_address}:latest" }
    end
  end

  describe '#previous_stable' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE').previous_stable }

      it { expect(release.image).to eq 'gitlab/gitlab-ce' }
      it { expect(release.tag).to eq 'latest' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE').previous_stable }

      it { expect(release.image).to eq 'gitlab/gitlab-ee' }
      it { expect(release.tag).to eq 'latest' }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.image).to eq 'gitlab/gitlab-ce' }
      it { expect(release.tag).to eq specific_ce_tag }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) do
          described_class.new(full_ce_address_with_simple_tag).previous_stable
        end

        it { expect(release.image).to eq 'gitlab/gitlab-ce' }
        it { expect(release.tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) do
          described_class.new(full_ce_address_with_complex_tag).previous_stable
        end

        it { expect(release.image).to eq 'gitlab/gitlab-ce' }
        it { expect(release.tag).to eq 'latest' }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) do
          described_class.new(full_ee_address_with_simple_tag).previous_stable
        end

        it { expect(release.image).to eq 'gitlab/gitlab-ee' }
        it { expect(release.tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) do
          described_class.new(full_ee_address_with_complex_tag).previous_stable
        end

        it { expect(release.image).to eq 'gitlab/gitlab-ee' }
        it { expect(release.tag).to eq 'latest' }
      end
    end

    context 'when release is a QA image' do
      subject(:release) do
        described_class.new("#{ce_qa_image}:nightly").previous_stable
      end

      it { expect(release.image).to eq ce_image }
      it { expect(release.tag).to eq 'latest' }
    end

    context 'when release is a full QA image' do
      subject(:release) do
        described_class.new("#{full_ce_qa_image}:nightly").previous_stable
      end

      it { expect(release.image).to eq ce_image }
      it { expect(release.tag).to eq 'latest' }
    end
  end

  describe '#edition' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.edition).to eq :ce }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.edition).to eq :ee }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.edition).to eq :ce }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.edition).to eq :ce }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.edition).to eq :ce }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.edition).to eq :ee }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.edition).to eq :ee }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.edition).to eq :ce }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.edition).to eq :ce }
    end
  end

  describe '#ee?' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release).not_to be_ee }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release).to be_ee }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release).not_to be_ee }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release).not_to be_ee }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release).not_to be_ee }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release).to be_ee }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release).to be_ee }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release).not_to be_ee }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release).not_to be_ee }
    end
  end

  describe '#to_ee' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.to_ee.to_s).to eq 'gitlab/gitlab-ee:nightly' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.to_ee.to_s).to eq release.to_s }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.to_ee.to_s).to eq "gitlab/gitlab-ee:#{specific_ce_tag}" }
    end

    context 'when tag includes `ce`' do
      subject(:release) { described_class.new('CE:abcdcef') }

      it { expect(release.to_ee.to_s).to eq 'gitlab/gitlab-ee:abcdcef' }
    end

    context 'when tag includes `ee`' do
      subject(:release) { described_class.new('CE:abcdeef') }

      it { expect(release.to_ee.to_s).to eq 'gitlab/gitlab-ee:abcdeef' }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.to_ee.to_s).to eq full_ee_address_with_simple_tag }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.to_ee.to_s).to eq "#{full_ee_address}:#{specific_ce_tag}" }
      end

      context 'with `ce` in the address outside of the image' do
        let(:ce_image) { 'registry.gitlab.com/cef/gitlab/gitlab-ce:latest' }
        let(:ee_image) { 'registry.gitlab.com/cef/gitlab/gitlab-ee:latest' }

        subject(:release) { described_class.new(ce_image) }

        it { expect(release.to_ee.to_s).to eq ee_image }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.to_ee.to_s).to eq release.to_s }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.to_ee.to_s).to eq release.to_s }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.to_ee.to_s).to eq "#{ee_image}:latest" }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.to_ee.to_s).to eq "#{full_ee_address}:latest" }
    end
  end

  describe '#image' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.image).to eq 'gitlab/gitlab-ce' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.image).to eq 'gitlab/gitlab-ee' }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.image).to eq 'gitlab/gitlab-ce' }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.image).to eq full_ce_address }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.image).to eq full_ce_address }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.image).to eq full_ee_address }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.image).to eq full_ee_address }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.image).to eq ce_image }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.image).to eq full_ce_address }
    end
  end

  describe '#qa_image' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.qa_image).to eq 'gitlab/gitlab-ce-qa' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.qa_image).to eq 'gitlab/gitlab-ee-qa' }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.qa_image).to eq "#{full_ce_address}-qa" }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.qa_image).to eq "#{full_ce_address}-qa" }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.qa_image).to eq full_ee_qa_image }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.qa_image).to eq full_ee_qa_image }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.qa_image).to eq ce_qa_image }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.qa_image).to eq full_ce_qa_image }
    end

    context 'when release is a dev image with gitlab ref in tag' do
      subject(:release) { described_class.new(full_dev_address_with_version_timestamp_refs_tag) }

      it { expect(release.qa_image).to eq dev_qa_address }
    end
  end

  describe '#project_name' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.project_name).to eq 'gitlab-ce' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.project_name).to eq 'gitlab-ee' }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.project_name).to eq 'gitlab-ce' }
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.project_name).to eq 'gitlab-ce' }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.project_name).to eq 'gitlab-ce' }
    end
  end

  describe '#tag' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.tag).to eq 'nightly' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.tag).to eq 'nightly' }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.tag).to eq specific_ce_tag }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.tag).to eq specific_ce_tag }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.tag).to eq specific_ee_tag }
      end
    end

    context 'when release is a QA image without a tag' do
      subject(:release) { described_class.new(ce_qa_image) }

      it { expect(release.tag).to eq 'latest' }
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:nightly") }

      it { expect(release.tag).to eq 'nightly' }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:nightly") }

      it { expect(release.tag).to eq 'nightly' }
    end
  end

  describe '#qa_tag' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.qa_tag).to eq 'nightly' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.qa_tag).to eq 'nightly' }
    end

    context 'when release is a full CE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.qa_tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.qa_tag).to eq specific_ce_qa_tag }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.qa_tag).to eq 'latest' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.qa_tag).to eq specific_ee_qa_tag }
      end
    end

    context 'when tag is already in the good form' do
      context 'with a simple tag' do
        subject(:release) { described_class.new('ce:11.0.8-rc8-ee') }

        it { expect(release.qa_tag).to eq '11.0.8-rc8-ee' }
      end

      context 'with a QA image and a numeric tag' do
        subject(:release) { described_class.new('gitlab/gitlab-ce-qa:11.1.8') }

        it { expect(release.qa_tag).to eq '11.1.8' }
      end
    end

    context 'when release is a QA image without a tag' do
      subject(:release) { described_class.new(ce_qa_image) }

      it { expect(release.qa_tag).to eq 'latest' }
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:nightly") }

      it { expect(release.qa_tag).to eq 'nightly' }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:nightly") }

      it { expect(release.qa_tag).to eq 'nightly' }
    end

    context 'when release is a dev image with gitlab ref in tag' do
      subject(:release) { described_class.new(full_dev_address_with_version_timestamp_refs_tag) }

      it { expect(release.qa_tag).to eq gitlab_ref }
    end
  end

  describe '#dev_gitlab_org?' do
    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release).not_to be_dev_gitlab_org }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release).not_to be_dev_gitlab_org }
    end

    context 'when release is a full address from non-dev' do
      subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

      it { expect(release).not_to be_dev_gitlab_org }
    end

    context 'when release is a full address from dev' do
      subject(:release) { described_class.new(full_dev_address) }

      it { expect(release).to be_dev_gitlab_org }
    end
  end

  describe '#omnibus_mirror?' do
    before do
      allow(Gitlab::QA::Runtime::Env).to receive(:ci_project_path).and_return("gitlab-org/gitlab")
    end

    context 'when image is a full ce address' do
      subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

      it { expect(release.omnibus_mirror?).to be false }
    end

    context 'when image is a dev image' do
      subject(:release) { described_class.new(full_dev_address) }

      it { expect(release.omnibus_mirror?).to be false }
    end

    context 'when image is omnibus-mirror image' do
      subject(:release) { described_class.new(omnibus_mirror_image) }

      it { expect(release.omnibus_mirror?).to be true }
      it { expect(release.qa_image).to eq("registry.gitlab.com/gitlab-org/gitlab/gitlab-ee-qa") }
    end
  end

  describe '#api_project_name' do
    context 'when release is ce' do
      subject(:release) { described_class.new('ce') }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end

    context 'when release is CE' do
      subject(:release) { described_class.new('CE') }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end

    context 'when release is ee' do
      subject(:release) { described_class.new('ee') }

      it { expect(release.api_project_name).to eq 'gitlab' }
    end

    context 'when release is EE' do
      subject(:release) { described_class.new('EE') }

      it { expect(release.api_project_name).to eq 'gitlab' }
    end

    context 'when release is EDITION:tag' do
      subject(:release) { described_class.new("CE:#{specific_ce_tag}") }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end

    context 'when release is edition:tag' do
      subject(:release) { described_class.new("ce:#{specific_ce_tag}") }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end

    context 'when release is a full CE address' do
      context 'without a tag' do
        subject(:release) { described_class.new(full_ce_address) }

        it { expect(release.api_project_name).to eq 'gitlab-foss' }
      end

      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ce_address_with_simple_tag) }

        it { expect(release.api_project_name).to eq 'gitlab-foss' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ce_address_with_complex_tag) }

        it { expect(release.api_project_name).to eq 'gitlab-foss' }
      end
    end

    context 'when release is a full EE address' do
      context 'with a simple tag' do
        subject(:release) { described_class.new(full_ee_address_with_simple_tag) }

        it { expect(release.api_project_name).to eq 'gitlab' }
      end

      context 'with a complex tag' do
        subject(:release) { described_class.new(full_ee_address_with_complex_tag) }

        it { expect(release.api_project_name).to eq 'gitlab' }
      end
    end

    context 'when release is a QA image' do
      subject(:release) { described_class.new("#{ce_qa_image}:latest") }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end

    context 'when release is a full QA image' do
      subject(:release) { described_class.new("#{full_ce_qa_image}:latest") }

      it { expect(release.api_project_name).to eq 'gitlab-foss' }
    end
  end
end
