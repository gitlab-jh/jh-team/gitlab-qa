# frozen_string_literal: true

require "logger"

describe Gitlab::QA::Support::GitlabUpgradePath do
  subject(:upgrade_path) { described_class.new(current_version, semver, "ee").fetch }

  let(:current_version) { "16.11.0-pre" }

  let(:tags) do
    <<~JSON
      [
        {"layer": "", "name": "latest"},
        {"layer": "", "name": "15.4.5-ee.0"},
        {"layer": "", "name": "15.11.6-ee.0"},
        {"layer": "", "name": "16.1.6-ee.0"},
        {"layer": "", "name": "16.3.3-ee.0"},
        {"layer": "", "name": "16.4.2-ee.0"},
        {"layer": "", "name": "16.7.5-ee.0"},
        {"layer": "", "name": "16.8.1-ee.0"},
        {"layer": "", "name": "16.10.2-ee.0"}
      ]
    JSON
  end

  let(:upgrade_path_yml) do
    <<~YML
      - major: 15
        minor: 4
      - major: 15
        minor: 11
      - major: 16
        minor: 1
        comments: "16.1 is a required step if you have [npm packages](https://docs.gitlab.com/ee/update/versions/gitlab_16_changes.html#1610)"
      - major: 16
        minor: 3
        comments: "16.0 is a required upgrade step for [instances with 30k Users](https://docs.gitlab.com/ee/update/versions/gitlab_16_changes.html#long-running-user-type-data-change)"
      - major: 16
        minor: 7
    YML
  end

  before do
    allow(Gitlab::QA::Runtime::Logger).to receive(:logger) { Logger.new(StringIO.new) }

    stub_request(:get, "https://registry.hub.docker.com/v2/namespaces/gitlab/repositories/gitlab-ee/tags?page=1&page_size=100")
      .to_return(status: 200, body: %({ "results": #{tags}, "next": null }))

    stub_request(:get, "https://gitlab.com/gitlab-org/gitlab/-/raw/master/config/upgrade_path.yml")
      .to_return(status: 200, body: upgrade_path_yml)
  end

  context "with upgrade from previous major" do
    let(:semver) { "major" }

    it "returns upgrade path between major versions" do
      expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:15.11.6-ee.0", "gitlab/gitlab-ee:16.1.6-ee.0",
        "gitlab/gitlab-ee:16.3.3-ee.0", "gitlab/gitlab-ee:16.7.5-ee.0"])
    end
  end

  context "with upgrade from previous minor" do
    let(:semver) { "minor" }

    it "returns upgrade path between minor versions" do
      expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:16.10.2-ee.0"])
    end
  end

  context "with patch upgrade" do
    let(:semver) { "patch" }

    context "when version exists in releases" do
      let(:current_version) { "16.10.2-pre" }

      it "returns upgrade path with current version" do
        expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:16.10.2-ee.0"])
      end
    end

    context "when version does not exist in releases" do
      let(:current_version) { "16.11.0-pre" }

      it "logs info message and exits" do
        logger_double = instance_double(Logger)
        allow(Gitlab::QA::Runtime::Logger).to receive(:logger).and_return(logger_double)
        allow(logger_double).to receive(:info)

        expect(logger_double).to receive(:info).with("Skipping upgrade test as version 16.11.0 is not yet released")
        expect { upgrade_path }.to raise_error(SystemExit)
      end
    end
  end

  context "with invalid version format" do
    let(:current_version) { "17.8-ee" }
    let(:semver) { "patch" }

    it "logs error and exits" do
      logger_double = instance_double(Logger)
      allow(Gitlab::QA::Runtime::Logger).to receive(:logger).and_return(logger_double)
      allow(logger_double).to receive(:error)

      expect(logger_double).to receive(:error).with("Invalid 'current_version' format: 17.8-ee. Expected format: MAJOR.MINOR.PATCH (e.g., 17.8.2)")
      expect { described_class.new(current_version, semver, "ee") }.to raise_error(SystemExit)
    end
  end

  context "with from_patch upgrade" do
    let(:semver) { "from_patch" }
    let(:tags) do
      <<~JSON
        [
          {"layer": "", "name": "latest"},
          {"layer": "", "name": "18.0.3-ee.0"},
          {"layer": "", "name": "18.0.2-ee.0"},
          {"layer": "", "name": "17.12.5-ee.0"},
          {"layer": "", "name": "17.12.0-ee.0"},
          {"layer": "", "name": "17.8.2-ee.0"},
          {"layer": "", "name": "17.7.4-ee.0"}
        ]
      JSON
    end

    context "when current version exists" do
      context "when next version exists" do
        let(:current_version) { "17.7.4-pre" }

        it "returns upgrade path to latest patch of next version" do
          expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:17.8.2-ee.0"])
        end
      end

      context "when no next version exists" do
        let(:current_version) { "18.0.3-pre" }

        it "logs info and exits" do
          logger_double = instance_double(Logger)
          allow(Gitlab::QA::Runtime::Logger).to receive(:logger).and_return(logger_double)
          allow(logger_double).to receive(:info)

          expect(logger_double).to receive(:info).with("Skipping upgrade test as next version after 18.0.3 is not yet available")
          expect { upgrade_path }.to raise_error(SystemExit)
        end
      end
    end

    context "when current version doesn't exist" do
      let(:current_version) { "18.1.0-pre" }

      it "logs info and exits" do
        logger_double = instance_double(Logger)
        allow(Gitlab::QA::Runtime::Logger).to receive(:logger).and_return(logger_double)
        allow(logger_double).to receive(:info)

        expect(logger_double).to receive(:info).with("Skipping upgrade test as version 18.1.0 is not yet released")
        expect { upgrade_path }.to raise_error(SystemExit)
      end
    end
  end
end
