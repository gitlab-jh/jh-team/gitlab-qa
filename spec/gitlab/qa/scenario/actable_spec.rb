# frozen_string_literal: true

describe Gitlab::QA::Scenario::Actable do
  subject(:actable) do
    Class.new do
      include Gitlab::QA::Scenario::Actable

      attr_accessor :something

      def do_something(arg = nil)
        "some#{arg}"
      end
    end
  end

  describe '.act' do
    it 'provides means to run steps' do
      result = actable.act { do_something }

      expect(result).to eq 'some'
    end

    it 'supports passing variables' do
      result = actable.act('thing') do |variable|
        do_something(variable)
      end

      expect(result).to eq 'something'
    end

    it 'returns value from the last method' do
      result = actable.act { 'test' }

      expect(result).to eq 'test'
    end
  end

  describe '.perform' do
    it 'makes it possible to pass binding' do
      variable = 'something'

      result = actable.perform do |object|
        object.something = variable
      end

      expect(result).to eq 'something'
    end
  end
end
