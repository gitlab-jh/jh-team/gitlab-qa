# frozen_string_literal: true

module Gitlab
  module QA
    describe Component::Gitaly do
      subject(:gitaly_component) { described_class.new }
      let(:full_ce_address) { 'registry.gitlab.com/foo/gitlab/gitlab-ce' }
      let(:full_ce_address_with_complex_tag) { "#{full_ce_address}:omnibus-7263a2" }

      describe '#release' do
        context 'with no release' do
          it 'defaults to CE' do
            expect(gitaly_component.release.to_s).to eq 'gitlab/gitlab-ce:nightly'
          end
        end
      end

      describe '#release=' do
        before do
          gitaly_component.release = release
        end

        context 'when release is a Release object' do
          let(:release) { Release.new('EE') }

          it 'returns a correct release' do
            expect(gitaly_component.release.to_s).to eq 'gitlab/gitlab-ee:nightly'
          end
        end

        context 'when release is a string' do
          context 'with a simple tag' do
            let(:release) { full_ce_address_with_complex_tag }

            it 'returns a correct release' do
              expect(gitaly_component.release.to_s).to eq full_ce_address_with_complex_tag
            end
          end
        end
      end

      describe '#name' do
        before do
          gitaly_component.release = Release.new('CE')
        end

        it 'returns a unique name' do
          expect(gitaly_component.name).to match(/\Agitaly-(\w+){8}\z/)
        end
      end

      describe '#reconfigure' do
        let(:docker) { spy('docker') }

        before do
          stub_const('Gitlab::QA::Support::ShellCommand', docker)
          gitaly_component.name = "gitaly-#{SecureRandom.hex(4)}"
        end

        it 'configures omnibus by writing gitlab.rb' do
          gitaly_component.reconfigure

          config = gitaly_component.gitaly_omnibus_configuration
          expect(docker).to have_received(:new).with(
            eq("docker exec #{gitaly_component.name} bash -c \"echo \\\"#{config}\\\" > /etc/gitlab/gitlab.rb;\""),
            anything
          )
        end
      end

      describe '#serverhooks' do
        let(:docker_engine) { spy('docker engine') }
        let(:test_name) { "test-server-hooks-#{SecureRandom.hex(5)}" }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
        end

        it 'adds git server hooks to the gitlab container' do
          gitaly_component.name = test_name

          expect(Support::ConfigScripts).to receive(:add_git_server_hooks).with(docker_engine, test_name)
          gitaly_component.process_exec_commands
        end
      end
    end
  end
end
