# frozen_string_literal: true

describe Gitlab::QA::Docker::Command do
  let(:shell_command) { spy('shell_command') }

  subject(:docker_command) { described_class.new }

  before do
    stub_const('Gitlab::QA::Support::ShellCommand', shell_command)
  end

  describe '#<<' do
    it 'appends command arguments' do
      docker_command << '--help'

      expect(docker_command.args).to include '--help'
    end

    it 'returns self' do
      expect(docker_command << 'args').to eq docker_command
    end
  end

  describe '#volume' do
    it 'appends volume arguments' do
      docker_command.volume('/from', '/to', 'Z')

      expect(docker_command.to_s).to include '--volume /from:/to:Z'
    end
  end

  describe '#env' do
    it 'appends env arguments with quotes' do
      docker_command.env('TEST', 'some value')

      expect(docker_command.to_s).to include '--env TEST="some value"'
    end
  end

  describe 'execute!' do
    it 'calls service shell_command' do
      expect(shell_command).to receive(:execute!)

      docker_command.execute!
    end
  end

  describe '.execute' do
    it 'executes command directly' do
      instance = double('command')
      expect(instance).to receive(:execute!)
      allow(described_class).to receive(:new).and_return(instance)

      described_class.execute('version')
    end
  end
end
